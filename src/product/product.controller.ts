import { Body, Controller, Delete, Get, Param, Post, Put, Req, Res } from "@nestjs/common";
import { Products } from "./product.model";
import { ProductService } from "./product.service";
import { Request, Response } from "express";

@Controller('api/v1/products')
export class ProductController{

     constructor(private readonly productsService: ProductService){}


     @Get()
     async getAllProducts(@Req() request:Request, @Res() response:Response ):Promise<any>{
          const result =  await this.productsService.getAllProducts()
          return response.status(200).json({
               status: "Ok!",
               message: "Successfully fetch data!",
               result: result 
          })
     }

     @Post()
     async postProducts(@Body() postData: Products):Promise<Products>{
          return this.productsService.createProducts(postData)
     }

     @Get(':id')
     async getProducts(@Param('id') id:number):Promise<Products | null>{
          return this.productsService.getProducts(id)
     }

     @Delete(':id')
     async deleteProducts(@Param('id') id:number):Promise<Products>{
          return this.productsService.deleteProducts(id)
     }

     @Put(':id')
     async updateProducts(@Param('id') id: number,@Body() data: Products): Promise<Products> {
       return this.productsService.updateProducts(id,data);
     }
}