import { PrismaService } from "src/prisma.service";
import { Products } from "./product.model";
import { Injectable } from "@nestjs/common";


@Injectable()
export class ProductService{

     constructor(private prisma: PrismaService){}

     async getAllProducts(): Promise<Products[]>{
          return this.prisma.products.findMany()
     }

     async getProducts(id:number): Promise<Products | null>{
          return this.prisma.products.findUnique({where: {id:Number(id)}})
     }

     async createProducts(data: Products): Promise<Products>{
          return this.prisma.products.create({
               data,
          })
     }

     async updateProducts(id:number,data:Products):Promise<Products>{
          return this.prisma.products.update({
               where: {id:Number(id)},
               data:{ title: data.title, description:data.description }
          })
     }

     async deleteProducts(id:number):Promise<Products>{
          return this.prisma.products.delete({
               where:{id: Number(id)}
          })
     }
}