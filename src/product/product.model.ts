import { Prisma } from "@prisma/client";


export class Products implements Prisma.ProductsCreateInput{
     id: number;
     title: string;
     description?: string;
}